<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::get();
        return view('page.index', compact('products'));
    }
    
    public function available()
    {
        $products = Product::where('stok', '>', 0)->get();
        return view('page.index', compact('products'));
    }

    public function unavailable()
    {
        $products = Product::where('stok', 0)->get();
        return view('page.index', compact('products'));
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('page.tambah');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //Validasi data
        $request->validate([
            'nama_produk' => 'required',
            'harga' => 'required',
            'stok' => 'required'
        ]);

        //Memasukan data
        $products = new Product;

        $products->nama_produk = $request['nama_produk'];
        $products->harga = $request['harga'];
        $products->stok = $request['stok'];

        $products->save();

        // Pesan berhasil
        // Alert::success(' BERHASIL ', ' Berhasil Menambah Produk! ');

        //kembali ke halaman index
        return redirect('/products');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $products = Product::findOrfail($id);
        return view('page.detail',compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(String $id)
    {
        $products = Product::findOrfail($id);
        return view('page.edit',compact('products'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_produk' => 'required',
            'harga' => 'required',
            'stok' => 'required',
        ]);

        $products = Product::findOrfail($id);

        $products->update([
            'nama_produk' => $request->nama_produk,
            'harga' => $request->harga,
            'stok' => $request->stok,
        ]);

        return redirect('/products');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $products = Product::findOrfail($id);
        $products->delete();

        return redirect('/products');
    }



    public function stock(Request $request, $id)
    {
        $this->validate($request, [
            'stok' => 'required',
        ]);

        $products = Product::findOrfail($id);

        $products->update([
            'stok' => $request->stok,
        ]);
        return redirect('/products/'.$id);
    }

}
