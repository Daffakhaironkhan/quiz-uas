@extends('layout.master')

@section('title')
    Halaman Edit Data Produk
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="card">
                <div class="card-body">
                    <h2>Formulir Edit Produk</h2>
                    <form action="/products/{{$products->id}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="nama_produk">Nama Produk</label>
                            <input type="text" class="form-control" id="nama_produk" name='nama_produk' value="{{$products->nama_produk}}">
                        </div>
                        @error('nama_produk')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="harga">Harga</label>
                            <input type="number" class="form-control" id="harga" name='harga' value="{{$products->harga}}">
                        </div>
                        @error('harga')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="stok">Stok</label>
                            <input type="number" class="form-control" id="stok" name='stok' value="{{$products->stok}}">
                        </div>
                        @error('stok')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection