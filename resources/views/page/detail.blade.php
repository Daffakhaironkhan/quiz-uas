@extends('layout.master')

@section('title')
    Halaman Detail Produk
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="card">
                <div class="card-body">
                    <h2>Nama Produk adalah {{$products->nama_produk}}</h2>
                    <p>Harga perbuahnya adalah Rp {{$products->harga}}</p>
                    <p>Stok saat ini sebanyak {{$products->stok}} buah</p>
                    <p>====================================================================</p>
                    <p>Apakah terdapat kesalahan? silahkan ubah data pada kolom dibawah ini</p>
                    <form action="{{route('update_stok',$products->id)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <input type="number" name="stok" value="{{$products->stok}}">
                        <input type="submit" class="btn btn-success btn-sm">
                    </form>
                    <a href="/products" class="btn btn-secondary btn-sm mt-5">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>      
@endsection