@extends('layout.master')

@section('title')
    Halaman Data Produk
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg">
                <div class="card">
                    <div class="card-body">
                        <a href="/products" type="button" class="btn btn-info mb-3">Semua Produk</a>
                        <a href="/products/available" type="button" class="btn btn-secondary mb-3">Produk Tersedia</a>
                        <a href="/products/unavailable" type="button" class="btn btn-danger mb-3">Produk Habis</a>
                        <a href="/products/create" type="button" class="btn btn-success mb-3">Tambah</a>
                        <table id="productsTables" class="table table-bordered table-striped data">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama Produk</th>
                                    <th scope="col">Harga</th>
                                    <th scope="col">Stok</th>
                                    <th scope="col">
                                        <center>Opsi</center>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($products as $key => $item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->nama_produk}}</td>
                                        <td>{{ $item->harga}}</td>
                                        <td>{{ $item->stok}}</td>
                                        <td>
                                            <center>
                                                <form action="/products/{{ $item->id }}" method='post'>
                                                    @csrf
                                                    @method('delete')
                                                    <a href="/products/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                                                    <a href="/products/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                                                    <button type="submit" class="btn btn-danger btn-sm show_confirm">Hapus</button>
                                                </form>
                                            </center>
                                        </td>
                                    </tr>
                                @empty
                                    <div class="alert alert-danger">
                                        Data Produk belum Tersedia.
                                    </div>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection