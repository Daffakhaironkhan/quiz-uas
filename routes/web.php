<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('layout.master');
});

// Route::put('/products/{product}/stock', [ProductController::class, 'stock'])->name('stock');
// Route::get('/products/available', [ProductController::class, 'available'])->name('available');
// Route::get('/products/unavailable', [ProductController::class, 'unavailable'])->name('unavailable');
// Route::get('/products',[ProductController::class,'index']);
// Route::get('/products/create',[ProductController::class,'create']);
Route::get('/products/available', [ProductController::class, 'available']);
Route::get('/products/unavailable', [ProductController::class, 'unavailable']);
Route::put('/products/{id}/stock', [ProductController::class, 'stock'])->name('update_stok');
Route::resource('/products', ProductController::class);